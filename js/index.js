$(function(){
    feather.replace()
    $('[data-toggle="popover"]').popover()
    $('[data-toggle="tooltip"]').tooltip()
    $('#contacto').on(
        'show.bs.modal',function (e){
            console.log('el modal contacto se está mostrando')                         
            $('#contactobtn').removeClass('btn-outline-warning');
            $('#contactobtn').addClass('btn-outline-success');
            $('#contactobtn').prop('disabled',true);
        }
        );
        $('#contacto').on(
            'shown.bs.modal',function(e){
                console.log('El modal contacto se mostró')
                $('#contactobtn').removeClass('btn-outline-success');
                $('#contactobtn').addClass('btn-success');
            }
        );
        $('#contacto').on(
            'hide.bs.modal',function(e){
                console.log('El modal contacto se oculta')
                $('#contactobtn').removeClass('btn-success');
                $('#contactobtn').addClass('btn-outline-success');
            }
        );
        $('#contacto').on(
            'hidden.bs.modal',function(e){
                console.log('El modal contacto se ocultó')
                $('#contactobtn').removeClass('btn-outline-success');
                $('#contactobtn').addClass('btn-outline-warning');
                $('#contactobtn').prop('disabled',false);
            }
        );
    });